# Grupo_3_004D


Links de interes

Iconos: https://ionicons.com/

Fuente: https://fonts.google.com/specimen/Titillium+Web?query=titi

NavBar: https://getbootstrap.com/docs/4.0/components/navbar/


Para utilizar bootstrap

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>


Para utilizar la fuente

<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Titillium+Web:wght@300;400;600&display=swap" rel="stylesheet">

Link hoja de estilos

<link rel="stylesheet" href="Estilos.css">

Para utilizar iconos
<script src="https://unpkg.com/ionicons@5.4.0/dist/ionicons.js"></script> *Se usa justo antes de que termine el body*