from django.apps import AppConfig


class AppLmgasConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app_lmgas'
