from django.urls import path, include
from .views import IniciarSesion, home

urlpatterns = [
    path('', home,name="home"),
    path('IniciarSesion/',IniciarSesion,name="IniciarSesion")
   
]
