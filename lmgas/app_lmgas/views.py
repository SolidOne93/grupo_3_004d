from django.shortcuts import render

# Create your views here.

def home(request):
    return render(request,"app_lmgas/home.html")

def IniciarSesion(request):
    return render(request,"app_lmgas/IniciarSesion.html")    
